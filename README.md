# DemoBank-FullStack

DemoBank is a simple banking platform and e-wallet that allows users to transfer funds, make deposits, and more. This project was initiated to enhance my understanding of React and Redux, while leveraging my existing knowledge of Spring Boot for the backend. Although it’s my first React project and some parts of the code might seem basic, it effectively demonstrates various banking functionalities.

![demobank_gif](demobank_gif.gif)

## Features

### User Features
- **User Registration**: Sign up for an account.
- **Profile Management**: Edit account details.
- **Multiple Accounts**: Manage multiple banking accounts.
- **Account Deletion**: Delete own account.
- **Deposit Funds**: Deposit money into accounts.
- **Fund Transfer**: Transfer funds between accounts.
- **Comment Section**: Write comments.
- **File Upload**: Upload relevant files.

### Admin Features
- **Account Deletion**: Delete a user's account.
- **File Access**: Download files uploaded by users.
- **Revoke Access**: Revoke user access while retaining their account for records.

## Running the DemoBank FullStack Project

### Prerequisites

- [Node.js](https://nodejs.org/) and [npm](https://www.npmjs.com/)
- [Java JDK](https://www.oracle.com/java/technologies/downloads/#java17) and [Maven](https://maven.apache.org/)
- Optional: [Docker](https://www.docker.com/get-started) and [docker-compose](https://docs.docker.com/compose/install/)

### Running Without Docker

1. **Backend (Spring Boot)**:
   ```bash
   cd backend
   ./mvnw spring-boot:run
   # On Windows: mvnw.cmd spring-boot:run
   ```
2. **Running the Frontend (React)**:
   ```bash
   cd frontend
   npm install
   npm start
   ```
   
### Setup & Run 
First, you need to clone the repository to your local machine:
   ```bash
   git clone https://gitlab.com/fullstackapplication/demobank.git
   docker-compose up --build
   ```

## Access the Application

Once the containers are up, you can access: 

- Frontend: Open your browser and navigate to http://localhost:3000 (or the port you specified in docker-compose.yml for the frontend).
- Backend: Should be running on http://localhost:8080.

 
