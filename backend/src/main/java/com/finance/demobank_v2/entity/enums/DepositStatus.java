package com.finance.demobank_v2.entity.enums;

public enum DepositStatus {
    PENDING, APPROVED,  REJECTED
}
