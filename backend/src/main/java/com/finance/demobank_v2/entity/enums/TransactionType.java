package com.finance.demobank_v2.entity.enums;

public enum TransactionType {
    TRANSFER,DEPOSIT
}
