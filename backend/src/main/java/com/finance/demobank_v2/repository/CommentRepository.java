package com.finance.demobank_v2.repository;

import com.finance.demobank_v2.entity.Account;
import com.finance.demobank_v2.entity.Comment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CommentRepository extends JpaRepository<Comment, Long> {
    List<Comment> findAllByAccount(Account account);
}
